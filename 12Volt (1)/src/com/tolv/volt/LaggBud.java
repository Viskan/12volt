package com.tolv.volt;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LaggBud
 */
@WebServlet("/LaggBud")
public class LaggBud extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LaggBud() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int auktionsId = Integer.parseInt(request.getParameter("AuktionId"));
		String userName = request.getParameter("UserName");
		double bud = Double.parseDouble(request.getParameter("Bud"));
		String sqlBud = "INSERT INTO tolvolt.bud VALUES (0, ?, ?, NOW(), ?)";
		

			try	{
				Class.forName("com.mysql.jdbc.Driver");
				Connection conn = DriverManager.getConnection("jdbc:mysql://eu-cdbr-azure-north-c.cloudapp.net:3306/tolvolt", "bb768fac2cc149", "0f616304");
				PreparedStatement stm =  conn.prepareStatement(sqlBud, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				
				if(getMaxBud(auktionsId, conn, bud)){
				
				stm.setInt(1, getKundId(userName, conn));
				stm.setInt(2, auktionsId);
				stm.setDouble(3, bud);

				 stm.executeUpdate();
				}

				stm.close();
				conn.close();

			} catch (ClassNotFoundException | SQLException e) {

				e.printStackTrace();
			}
		

		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}


	private int getKundId(String username, Connection conn){
		String sqlKundId = "SELECT id FROM tolvolt.Kund where Kund.Epost=?";
		int id = 0;


		try {
			Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement stm =  conn.prepareStatement(sqlKundId, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

			stm.setString(1, username);
			ResultSet rs = stm.executeQuery();
			rs.last();

			id = rs.getInt("id");


			stm.close();
			rs.close();

		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}

	private boolean getMaxBud(int auktionsId, Connection conn, double givenBud){

		String sqlKundId = "SELECT MAX(Bud) AS 'Bud' FROM tolvolt.Bud where Bud.AuktionId=?";
		boolean returnValue = false;
		Double bud = 0.0;


		try {
			Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement stm =  conn.prepareStatement(sqlKundId, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

			stm.setInt(1, auktionsId);
			ResultSet rs = stm.executeQuery();

			rs.last();
			bud = rs.getDouble("Bud");


			stm.close();
			rs.close();

		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(bud > 0.0){
			if(bud < givenBud){
				returnValue = true;
			}else{
				returnValue = false;
			}
		}else{
			returnValue = true;
		}
		return returnValue;
	}

}
