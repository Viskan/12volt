package com.tolv.volt;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Loggin
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection conn = DriverManager.getConnection("jdbc:mysql://eu-cdbr-azure-north-c.cloudapp.net:3306/tolvolt", "bb768fac2cc149", "0f616304");
			PreparedStatement stm = conn.prepareStatement("SELECT * FROM Kund WHERE Epost=? AND Lösenord=?");
			stm.setString(1, email);
			stm.setString(2, password);
			ResultSet rs = stm.executeQuery();
			rs.last();
			if(rs.getRow() > 0) {
				Cookie userCookie = new Cookie("userCookie", rs.getString("Epost"));
				userCookie.setMaxAge(60 * 60);
	            response.addCookie(userCookie);
				response.sendRedirect("index.jsp");
			} else {
				response.sendRedirect("login.jsp");
			}
			
			rs.close();
			stm.close();
			conn.close();
						
		} catch (Exception e) {
			e.printStackTrace();
		response.sendRedirect("login.jsp");
		}
	}
		
	

}
