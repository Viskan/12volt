package com.tolv.volt;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

class Kategori{
	String namn;
	String beskrivning;
	String starttid;
	String sluttid;
	String kategori;
	String leverantorId;
	int id;
	double acceptpris;
	double bud;
	String image;
}

@WebServlet("/HamtaEfterKategori")
public class HamtaEfterKategori extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String kategori = request.getParameter("kategori");
		
		try {
            kategori = new String(kategori.getBytes("ISO-8859-1"), "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
        }

		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://eu-cdbr-azure-north-c.cloudapp.net:3306/tolvolt", "bb768fac2cc149", "0f616304");
			PreparedStatement pstm = conn.prepareStatement("select auktion.id, auktion.namn as Produkt, max(bud.bud) as bud, kategori.namn as Kategori, auktion.sluttid, auktion.AcceptPris as Acceptpris, auktion.bild from auktion"
					+ " inner join auktionkategori on auktion.id = auktionkategori.auktionid"
					+ " inner join kategori on auktionkategori.kategoriid = kategori.id"
					+ " left join bud on auktion.id = bud.AuktionId"
					+ " WHERE kategori.id = (SELECT Id FROM kategori WHERE namn=?)"
					+ " group by produkt");
			
			pstm.setString(1, kategori);
			ResultSet rs = pstm.executeQuery();

			ArrayList<Kategori> productList = new ArrayList<Kategori>();
			while(rs.next()){
				Kategori p = new Kategori();
				p.namn = rs.getString("produkt");
				p.sluttid = rs.getString("sluttid");
				p.bud = rs.getDouble("bud");
				p.id = rs.getInt("id");
				Blob bild = rs.getBlob("bild");
				p.kategori = rs.getString("kategori");
				p.acceptpris = rs.getDouble("acceptpris");
				p.image = javax.xml.bind.DatatypeConverter.printBase64Binary(bild.getBytes(1, (int)bild.length()));
				productList.add(p);
			}
			rs.close();
			pstm.close();
			conn.close();
			
			Gson gson = new Gson();
			String json = gson.toJson(productList.toArray());
			
			PrintWriter out = response.getWriter();
			out.println(json);
		}catch(Exception e){

		}
	}

}
