package com.tolv.volt;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;


class Produkt{
	String namn;
	String beskrivning;
	String starttid;
	String sluttid;
	String kategori;
	String image;
	String leverantorId;
	int id;
	double acceptpris;
	double bud;
}

@WebServlet("/HamtaProdukt")
public class HamtaProdukt extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		
		String id = request.getParameter("id");
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://eu-cdbr-azure-north-c.cloudapp.net:3306/tolvolt", "bb768fac2cc149", "0f616304");
			PreparedStatement pstm = conn.prepareStatement("select auktion.id, auktion.namn as Produkt, max(bud.bud) as bud, kategori.namn as Kategori, auktion.Beskrivning, auktion.sluttid, auktion.starttid, auktion.bild,"
					+ " auktion.AcceptPris as Acceptpris from auktion"
					+ " inner join auktionkategori on auktion.id = auktionkategori.auktionid"
					+ "	inner join kategori on auktionkategori.kategoriid = kategori.id"
					+ "	left join bud on auktion.id = bud.AuktionId"
					+ "	where auktion.id = ?"
					+ " group by produkt;");
			
			pstm.setString(1, id);
			ResultSet rs = pstm.executeQuery();

					ArrayList<Auktion> productList = new ArrayList<Auktion>();
					while(rs.next()){
						Auktion p = new Auktion();
						p.namn = rs.getString("produkt");
						p.starttid = rs.getString("starttid");
						p.sluttid = rs.getString("sluttid");
						p.beskrivning = rs.getString("beskrivning");
						p.bud = rs.getDouble("bud");
						p.id = rs.getInt("id");
						Blob bild = rs.getBlob("bild");
						p.kategori = rs.getString("kategori");
						p.acceptpris = rs.getDouble("acceptpris");
						p.image = javax.xml.bind.DatatypeConverter.printBase64Binary(bild.getBytes(1, (int)bild.length()));
						productList.add(p);
					}
					rs.close();
					pstm.close();
					conn.close();

					Gson gson = new Gson();
					String json = gson.toJson(productList.toArray());

					PrintWriter out = response.getWriter();
					out.println(json);

		}catch(Exception e){
			e.printStackTrace();
		}
	}
}


