package com.tolv.volt;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

class BudHistorik{
	String tid;
	int kundId;
	double bud;
}

@WebServlet("/HamtaBudHistorik")
public class HamtaBudHistorik extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");

		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://eu-cdbr-azure-north-c.cloudapp.net:3306/tolvolt", "bb768fac2cc149", "0f616304");
			PreparedStatement pstm = conn.prepareStatement("SELECT bud.Tid, bud.KundId, bud.Bud FROM tolvolt.bud WHERE bud.AuktionId = ? ORDER BY bud.Bud DESC ;");
			pstm.setString(1, id);
			ResultSet rs = pstm.executeQuery();

			ArrayList<BudHistorik> budList = new ArrayList<BudHistorik>();
			while(rs.next()){
				BudHistorik p = new BudHistorik();
				p.tid = rs.getString("tid");
				p.kundId = rs.getInt("kundid");
				p.bud = rs.getDouble("bud");
				budList.add(p);
			}
			rs.close();
			pstm.close();
			conn.close();
			
			Gson gson = new Gson();
			String json = gson.toJson(budList.toArray());
			
			PrintWriter out = response.getWriter();
			out.println(json);
		}catch(Exception e){

		}
	}

}
