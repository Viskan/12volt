<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String userName = null;
	Cookie[] cookies = request.getCookies();
	if (cookies != null) {
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("userCookie"))
				userName = cookie.getValue();
		}
	}
	if (userName == null)
		response.sendRedirect("login.jsp");
%>
<!DOCTYPE html>
<html>
<head>
<title>12</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
	<%@ page import="java.sql.*"%>
	<%
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager
				.getConnection(
						"jdbc:mysql://eu-cdbr-azure-north-c.cloudapp.net:3306/tolvolt",
						"bb768fac2cc149", "0f616304");
		Statement stm = con.createStatement();
		ResultSet rs = stm
				.executeQuery("SELECT kategori.namn, COUNT(auktionkategori.KategoriId) AS antal FROM kategori LEFT JOIN auktionkategori ON kategori.Id=auktionkategori.KategoriId GROUP BY kategori.Id");
	%>
	<main> <header>
		<div class="row">
			<div class="col-sm-7">
				<h1>Nackademiska Auktionsfrämjandet</h1>
			</div>
			<div class="col-sm-5">
				<img src="img/nyklubba.png">
			</div>
		</div>
	</header>
	<nav>
		<ul class="nav nav-tabs">
			<li role="presentation" class="active"><a href="index.jsp">Auktioner</a></li>
			<li role="presentation"><a href="kontakt.jsp">Kontakt</a></li>
			<li role="presentation"><a href="Logout">Logout</a></li>
		</ul>
	</nav>
	<section>
		<br> <select class="form-control" name="kategori">
			<option>Alla auktioner</option>
			<%
				while (rs.next()) {
			%>
			<option>
				<%=rs.getString("namn")%> (<%=rs.getInt("antal")%>)
			</option>
			<%
				}
				rs.close();
				stm.close();
				con.close();
			%>
		</select>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th></th>
					<th>Produkt</th>
					<th>Kategori</th>
					<th>Sluttid</th>
					<th>Aktuellt bud</th>
					<th>Acceptpris</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="alist">

			</tbody>
		</table>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel"></h4>
					</div>
					<div class="modal-body">
						<table class="table table-striped">
							<tbody id="modal-list">
							</tbody>
						</table>
						<div class="panel-group" id="accordion" role="tablist"
							aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion"
											href="#collapseOne" aria-expanded="true"
											aria-controls="collapseOne">Budhistorik:</a>
											
									</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">

										<table class="table">
											<thead>
												<tr>
													<th>Tid</th>
													<th>Budgivare</th>
													<th>Bud</th>
												</tr>
											</thead>
											<tbody id="budHistorik">
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div id="budField">
							<div id="budField">
								<input id="givenBud" name="Bud" type="text">
								<button id="addBudButton" class="btn btn-primary" >Lägg bud</button>
								<button id="buyNowButton" type="button" class="btn btn-primary"
									data-dismiss="modal"></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	</main>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/script.js"></script>
</body>
</html>