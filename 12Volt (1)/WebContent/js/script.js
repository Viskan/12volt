var globalId;

$(document).ready(function(){
	allaAuktioner();
});

function allaAuktioner(){

	$("#alist").html("");
	
	$.getJSON("/12Volt/HamtaAuktioner?", function(data){
		$.each(data, function(i, product){
			if(product.bud < product.acceptpris){
				$("#alist").append("<tr data-toggle=\"modal\" onClick=\"hamtaSak(" + product.id + ");\" data-target=\"#myModal\" pid=\"" + product.id + "\" >"
						+"<td>" + "<img class=\"thumbnails\" src=\"data:image/jpeg;base64, " + product.image + "\"></td>"
						+"<td>" + product.namn +"</td>"
						+"<td>" + product.kategori +"</td>"
						+"<td>" + product.sluttid +"</td>"
						+"<td>" + product.bud +"kr</td>"
						+"<td>" + product.acceptpris +"kr</td>"
						+"</tr>");
			}
		});
	});
}

function hamtaSak(id){
	globalId = id;
	var produktId = id;
	$("#myModalLabel").html("");
	$("#modal-list").html("");
	$("#buyNowButton").html("");
	$("#budHistorik").html("");

	$.getJSON("/12Volt/HamtaProdukt?id=" + id, function(data){
		$.each(data, function(i, product){

			$("#myModalLabel").html(product.namn);

			$("#modal-list").html("<tr>" +
					"</tr>" +
					"<tr>" +
					"<td><div id=\"invis\"><img class=\"img-responsive\" src=\"data:image/jpeg;base64, " + product.image + "\"></div></td>" +
					"<td>" + product.beskrivning + "</td>" +
					"</tr>" +
					"<tr>" +
					"<td>Acceptpris:</td>" +
					"<td>" + product.acceptpris +"kr</td>" +
					"</tr>" +
					"<tr>" +
					"<td>Kategori:</td>" +
					"<td>" + product.kategori +"</td>" +
					"</tr>" +
					"<tr>" +
					"<td>Startade:</td>" +
					"<td>" + product.starttid +"</td>" +
					"</tr>" +
					"<tr>" +
					"<td>Slutar:</td>" +
					"<td>" + product.sluttid +"</td>" +
					"</tr>" +
					"<tr>" +
					"<td>Högsta bud:</td>" +
					"<td>" + product.bud +"kr</td>" +
			"</tr>");

			$("#givenBud").val(product.bud+25.00);
			$("#buyNowButton").html("Lägg acceptpris: " + product.acceptpris + " kr");
		});
	});

	$.getJSON("/12Volt/HamtaBudHistorik?id=" + produktId, function(data){
		$.each(data, function(i, bud){
			$("#budHistorik").append("<tr>" +
					"<td>" + bud.tid + "</td>" +
					"<td>" + bud.kundId + "</td>" +
					"<td>" + bud.bud + "</td>" +
			"</tr>");
		});
	});
}

$(".form-control").change(function () {
	$("#alist").html("");
	var katSplitArray = ($(".form-control option:selected").text()).split('(');
	var kat = katSplitArray[0];
	if(kat === "Alla auktioner"){
		allaAuktioner();
	} else {
		$.getJSON("/12Volt/HamtaEfterKategori?kategori=" + kat, function(data){
			$.each(data, function(i, product){
				$("#alist").append("<tr data-toggle=\"modal\" onClick=\"hamtaSak(" + product.id + ");\" data-target=\"#myModal\" pid=\"" + product.id + "\" >"
						+"<td>" + "<img class=\"thumbnails\" src=\"data:image/jpeg;base64, " + product.image + "\"></td>"
						+"<td>" + product.namn +"</td>"
						+"<td>" + product.kategori +"</td>"
						+"<td>" + product.sluttid +"</td>"
						+"<td>" + product.bud +"kr</td>"
						+"<td>" + product.acceptpris +"kr</td>"
						+"</tr>");
			});
		});
	}
});

$("#addBudButton").click(function(){
	var userName = $.cookie("userCookie");
	var bud = $("#givenBud").val();
	$.post("LaggBud",{AuktionId: globalId, UserName: userName, Bud: bud}, function(){
		hamtaSak(globalId);
		allaAuktioner();
	}); 

});

$("#buyNowButton").click(function(){
	var userName = $.cookie("userCookie");
	var budArr = $("#buyNowButton").text().split(" ");
	var bud = budArr[2];
	$.post("LaggBud",{AuktionId: globalId, UserName: userName, Bud: bud}, function(){
		hamtaSak(globalId);
		allaAuktioner();
	});
});