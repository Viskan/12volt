<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String userName = null;
	Cookie[] cookies = request.getCookies();
	if (cookies != null) {
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("userCookie"))
				userName = cookie.getValue();
		}
	}
	if (userName != null)
		response.sendRedirect("index.jsp");
%>
<!DOCTYPE html>
<html>
<head>
<title>12</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<main> <header>
		<div class="row">
			<div class="col-sm-7">
				<h1>Nackademiska Auktionsfrämjandet</h1>
			</div>
			<div class="col-sm-5">
				<img src="img/nyklubba.png">
			</div>
		</div>
	</header>
	<nav>
		<ul class="nav nav-tabs">
			<li role="presentation" class="active"><a href="login.jsp">Logga
					in</a></li>
		</ul>
	</nav>
	<section>
		<div id="loginBox">
			<form class="form-signin" action="Login" method="post">
				<label for="inputEmail" class="sr-only">Epost-adress</label> <input
					type="email" id="inputEmail" class="form-control"
					placeholder="Epost-adress" name="email" required autofocus>
				<label for="inputPassword" class="sr-only">Lösenord</label> <input
					type="password" id="inputPassword" class="form-control"
					placeholder="Lösenord" name="password" required>
				<button class="btn btn-lg btn-primary btn-block" type="submit">Logga
					in</button>
			</form>
			<br>

		</div>

	</section>
	</main>
</body>
</html>