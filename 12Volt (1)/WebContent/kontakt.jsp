<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String userName = null;
	Cookie[] cookies = request.getCookies();
	if (cookies != null) {
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("userCookie"))
				userName = cookie.getValue();
		}
	}
	if (userName == null)
		response.sendRedirect("login.jsp");
%>
<!DOCTYPE html>
<html>
<head>
<title>12</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<main> <header>
		<div class="row">
			<div class="col-sm-7">
				<h1>Nackademiska Auktionsfrämjandet</h1>
			</div>
			<div class="col-sm-5">
				<img src="img/auktionsklubba.png">
			</div>
		</div>
	</header>
	<nav>
		<ul class="nav nav-tabs">
			<li role="presentation"><a href="index.jsp">Auktioner</a></li>
			<li role="presentation" class="active"><a href="kontakt.jsp">Kontakt</a></li>
			<li role="presentation"><a href="Logout">Logout</a></li>
		</ul>
	</nav>
	<section>
		<div id="content">
			<br>

			<div id="kontaktleft">
				<fieldset class="rutor">
					<legend>Adress</legend>

					<div id="adresser">
						<label class="texter">Auktionsgatan 42 </label><br> <label
							class="texter">172 35</label><br> <label class="texter">Stockholm</label>
					</div>


					<br>
				</fieldset>
				<br>

				<fieldset class="rutor">
					<legend>Kontaktuppgifter</legend>
					<label class="texter">Lucas: 0733367625</label><br> <label
						class="texter">Olle: 0708334421</label><br> <label
						class="texter">Reception: 0867032322</label>

				</fieldset>
				<br> <br>
			</div>
			<div id="kontaktright">
				<fieldset class="rutor">
					<legend>Telefontider</legend>
					<label class="texter">Måndag-fredag: 09.00-16.00</label><br> <label
						class="texter">Lördag: 10.00-15.00</label><br> <label
						class="texter">Söndag: Stängt</label>


				</fieldset>
				<br> <br> <br> <b></b><span id="namnspan"></span><br>

			</div>

		</div>
	</section>
	</main>
</body>
</html>